# Greeting chrome extension

## Installation

1) Download project as ```.zip``` file
2) Extract ```test-chrome-extension-main``` folder
3) Go to chrome extensions page
4) Enable Developer Mode
5) Install extension and disable Developer Mode.
