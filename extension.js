const greetings =
[
    "<span>Hello!</span>",
    "<span>Добрый</span> <span>день!</span>",
    "<span>Bonjour!</span>",
    "<span>Buenos</span> <span>días!</span>",
    "<span>Guten</span> <span>Tag.</span>",
    "<span>Buongiorno!</span>",
    "<span>Olá!</span>",
    "<span>Dzień</span> <span>dobry!</span>",
    "<span>नमस्ते</span>",
    "<span>Hyvää</span> <span>päivää</span>",
    "<span>Γειά</span> <span>σας</span>"
];

setInterval (function () {
    document.getElementById ("text-container").innerHTML = greetings[Math.floor (Math.random () * greetings.length)];
}, 3600);
